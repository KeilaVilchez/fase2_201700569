﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VistaAdministrador.aspx.cs" Inherits="IPC2F2_201700569.VistaAdministrador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    background-image: url('https://i.pinimg.com/originals/68/98/0d/68980d0d4fdb084a8d384d8a20a3e607.jpg');
}

.form-register {
    width: 629px;
    background: #24303c;
    padding: 30px;
    margin-top: 100px;
    border-radius: 4px;
    font-family: 'calibri';
    color: white;
    box-shadow: 7px 13px 37px #000;
              height: 310px;
              margin-left: auto;
              margin-right: auto;
              margin-bottom: auto;
          }

    .form-register h4 {
        font-size: 22px;
        margin-bottom: 20px;
    }

.controls {
    width: 100%;
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}

.controls1{
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}
.form-register p {
    height: 40px;
    text-align: center;
    font-size: 18px;
    line-height: 40px;
}

.form-register a {
    color: white;
    text-decoration: none;
}

    .form-register a:hover {
        color: white;
        text-decoration: underline;
    }

.form-register .botons {
        border-style: none;
            border-color: inherit;
            border-width: medium;
            background: #000000;
            padding: 12px;
            color: white;
    margin: 0px 0 16px 0;
        }
</style>
</head>
<body>
     <form id="form1" runat="server">
    <section class="form-register">
    <h4> Administrador&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <asp:Button class="botons" ID="Button9" runat="server" Text="Cerrar Sesion" Width="120px"  Height="44px" /></h4>
      
        <p>
            <asp:Button class="botons" ID="Button3" runat="server" Text="Tipo Empleado" Width="125px"  Height="63px" />
            <asp:Button class="botons" ID="Button1" runat="server" Text="Empleado" Width="125px"  Height="63px" />
            <asp:Button class="botons" ID="Button2" runat="server" Text="Productos" Width="125px"  Height="63px" />
       
            
            <asp:Button class="botons" ID="Button4" runat="server" Text="Categoria P" Width="125px"  Height="63px" />
     
            </p>
        <p></p>
         <p>
            <asp:Button class="botons" ID="Button5" runat="server" Text="Ciudad" Width="125px"  Height="63px" />
            <asp:Button class="botons" ID="Button6" runat="server" Text="Departamento" Width="125px"  Height="63px" />
            <asp:Button class="botons" ID="Button7" runat="server" Text="Asignar Meta" Width="125px"  Height="63px" />
       
            
            <asp:Button class="botons" ID="Button8" runat="server" Text="Cargar XML " Width="125px"  Height="63px" />
     
            </p>
  </section>
    </form>
</body>
</html>
