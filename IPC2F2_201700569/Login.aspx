﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="IPC2F2_201700569.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    background-image: url('https://i.pinimg.com/originals/5a/25/47/5a25473008acaa6f17576f77d23c7740.jpg');
}

.form-register {
    width: 400px;
    background: ##D3D3D3;
    padding: 30px;
    margin: auto;
    margin-top: 100px;
    border-radius: 4px;
    font-family: 'calibri';
    color: white;
    box-shadow: 7px 13px 37px #000;
}

    .form-register h4 {
        font-size: 22px;
        margin-bottom: 20px;
    }

.controls {
    width: 100%;
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}

.controls1{
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}
.form-register p {
    height: 40px;
    text-align: center;
    font-size: 18px;
    line-height: 40px;
}

.form-register a {
    color: white;
    text-decoration: none;
}

    .form-register a:hover {
        color: white;
        text-decoration: underline;
    }

.form-register .botons {
        border-style: none;
            border-color: inherit;
            border-width: medium;
            background: #24303c;
            padding: 12px;
                color: white;
    margin: 0px 0 16px 0;
   

   /* border-radius: 15px;*/
    border: 3px double #A9A9A9;
        }

</style>
</head>
<body>
     <form id="form1" runat="server">
    <section class="form-register">
    <h4> Login</h4>
      
         <asp:TextBox class="controls" ID="TextBox1" runat="server" autofocus="true"  placeholder="Ingrese su usuario"  ></asp:TextBox>
         <asp:TextBox class="controls" ID="TextBox2" runat="server" autofocus="true"  placeholder="Ingrese su contraseña" TextMode="Password" ></asp:TextBox>
      
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
            <asp:Button class="botons" ID="Button3" runat="server" Text="Ingresar" Width="75px" OnClick="Button3_Click" />

            
       
       </p><asp:Label ID="Label1" runat="server"> </asp:Label>
  </section>
    </form>
</body>
</html>
