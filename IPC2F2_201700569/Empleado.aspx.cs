﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IPC2F2_201700569
{
    public partial class Empleado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //IniciarLLenadoDropDownList();
            Button3.Enabled = false;
        }
        // agregar dato
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Convert.ToInt32(DDLPuesto.Text)
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    if (string.IsNullOrEmpty(TextBox9.Text))
                    {
                        string jefe = "null";
                        sqlCon.Open();
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado (Nit, nombre, apellido, nacimiento, direccion, telefono, celular, email, id_puesto, jefe, contrasena) VALUES ('" + TextBox1.Text + "' , '" + TextBox2.Text + "','" + TextBox3.Text + "', '" + TextBox4.Text + "','" + TextBox5.Text + "','" + TextBox6.Text + "','" + TextBox7.Text + "','" + TextBox8.Text + "'," + TextBox10.Text + "," + jefe + ",'" + TextBox11.Text + "')", sqlCon);
                        DataTable dtbl = new DataTable();

                        sqlDa.Fill(dtbl);
                        sqlCon.Close();

                        Label1.Text = "El registro " + TextBox1.Text + " fue guardado con exito";
                        eliminar();
                    }
                    else if (string.IsNullOrEmpty(TextBox1.Text)& string.IsNullOrEmpty(TextBox2.Text)& string.IsNullOrEmpty(TextBox3.Text) & string.IsNullOrEmpty(TextBox4.Text)& string.IsNullOrEmpty(TextBox5.Text) & string.IsNullOrEmpty(TextBox6.Text)& string.IsNullOrEmpty(TextBox7.Text) & string.IsNullOrEmpty(TextBox8.Text)& string.IsNullOrEmpty(TextBox10.Text) & string.IsNullOrEmpty(TextBox11.Text))
                    {
                        Label1.Text = "Algunos de los datos obligatorios no se han llenado, intentar nuevamente";
                    }
                    else
                            {
                        sqlCon.Open();
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado (Nit, nombre, apellido, nacimiento, direccion, telefono, celular, email, id_puesto, jefe, contrasena) VALUES ('" + TextBox1.Text + "' , '" + TextBox2.Text + "','" + TextBox3.Text + "', '" + TextBox4.Text + "','" + TextBox5.Text + "','" + TextBox6.Text + "','" + TextBox7.Text + "','" + TextBox8.Text + "'," + TextBox10.Text + ",'" + TextBox9.Text + "','" + TextBox11.Text + "')", sqlCon);
                        DataTable dtbl = new DataTable();

                        sqlDa.Fill(dtbl);
                        sqlCon.Close();

                        Label1.Text = "El registro " + TextBox1.Text + " fue guardado con exito";
                        eliminar();
                    }

                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
           
        }
        // buscando dato para el DDL para puesto
        //public DataSet Consultar(string Consulta)
        //{

        //    string connectionString = @"Data Source=DESKTOP-PHUI5MP; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
        //    using (SqlConnection sqlCon = new SqlConnection(connectionString))
        //    {
        //        sqlCon.Open();
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(Consulta, sqlCon);
        //        DataSet dtbl = new DataSet();

        //        sqlDa.Fill(dtbl);
        //        //sqlCon.Close();
        //        return dtbl;
        //        //Label1.Text = "El registro " + TextBox1.Text + " fue guardado con exito";
        //    }

        //}
    
        //// llenar el DDL de puesto
        //private void IniciarLLenadoDropDownList()
        //{
            

          
        //    //Datos Puesti
        //    DDLPuesto.DataSource = Consultar("Select id_puesto, nombre from Puesto");
        //    DDLPuesto.DataTextField = "id_puesto";
        //    DDLPuesto.DataValueField = "id_puesto";
        //    DDLPuesto.DataBind();
        //    DDLPuesto.Items.Insert(0, new ListItem("[Seleccionar]", "0"));


        //}
     



     
       

        //Para eliminar datos
        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("Delete from Empleado WHERE Nit='" + TextBox12.Text + "'", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    

                    Label1.Text = "El Empleado " + TextBox12.Text + " fue Eliminado con exito";
                    eliminar();
                    sqlCon.Close();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }

        }

        //para mostrar datos
        protected void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT Nit, nombre, apellido, direccion, celular, email, jefe FROM Empleado ", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    gvPhoneBook.DataSource = dtbl;
                    gvPhoneBook.DataBind();

                }
            }
            catch(Exception ex)
            {
                Response.Write(ex.Message);
            }

        }

        //buscar dato para modificar
        protected void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "SELECT Nit, nombre, apellido, nacimiento, direccion, telefono, celular, email, id_puesto, jefe, contrasena FROM Empleado where  Nit ='" + TextBox13.Text + "'";
                    SqlCommand comando = new SqlCommand(cadena, sqlCon);
                    SqlDataReader registro = comando.ExecuteReader();
                    if (registro.Read())
                    {
                        TextBox1.Text = registro["Nit"].ToString();
                        TextBox2.Text = registro["nombre"].ToString();
                        TextBox3.Text = registro["apellido"].ToString();
                        TextBox4.Text = registro["nacimiento"].ToString();
                        TextBox5.Text = registro["direccion"].ToString();
                        TextBox6.Text = registro["telefono"].ToString();
                        TextBox7.Text = registro["celular"].ToString();
                        TextBox8.Text = registro["email"].ToString();
                        //DDLPuesto.Text = registro["id_puesto"].ToString();
                        TextBox10.Text = registro["id_puesto"].ToString();
                        TextBox9.Text = registro["jefe"].ToString();
                        TextBox11.Text = registro["contrasena"].ToString();
                        Button3.Enabled = true;
                        TextBox1.Enabled = false;
                        TextBox9.Enabled = false;
                        TextBox10.Enabled = false;

                    }
                    else
                        //MessageBox.Show("No existe un artículo con el código ingresado");
                    sqlCon.Close();

                }
            }
            catch(Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        //para modificar
        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {

                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "update Empleado set nombre='" + TextBox2.Text + "',apellido='" + TextBox3.Text + "', nacimiento='" + TextBox4.Text + "' , direccion='" + TextBox5.Text + "', telefono='" + TextBox6.Text + "', celular='" + TextBox7.Text + "', email='" + TextBox8.Text + "', contrasena='" + TextBox11.Text + "'where Nit ='" + TextBox13.Text + "'";
                    SqlDataAdapter sqlDa = new SqlDataAdapter(cadena, sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();

                    Label1.Text = "El Empleado con Nit " + TextBox13.Text + " fue modificado con exito";
                    TextBox1.Enabled = true;
                    TextBox9.Enabled = true;
                    TextBox10.Enabled = true;
                    eliminar();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }

        public void eliminar()
        {
            TextBox13.Text = " ";
            TextBox1.Text = " ";
            TextBox2.Text = " ";
            TextBox3.Text = " ";
            TextBox4.Text = " ";
            TextBox5.Text = " ";
            TextBox6.Text = " ";
            TextBox7.Text = " ";
            TextBox8.Text = " ";
            //DDLPuesto.Text = " ";
            TextBox9.Text = " ";
            TextBox10.Text = " ";
            TextBox11.Text = " ";
            TextBox12.Text = " ";
            TextBox13.Text = " ";
        }
    }
}