﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
namespace IPC2F2_201700569
{
    public partial class Departamento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //Buscar dato
        protected void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "SELECT id_departamento, nombredepartamento, id_ciudad FROM Departamento where  id_departamento =" + TextBox13.Text;
                    SqlCommand comando = new SqlCommand(cadena, sqlCon);
                    SqlDataReader registro = comando.ExecuteReader();
                    if (registro.Read())
                    {
                        TextBox1.Text = registro["id_departamento"].ToString();
                        TextBox2.Text = registro["nombredepartamento"].ToString();
                        TextBox3.Text = registro["id_ciudad"].ToString();
                        Button3.Enabled = true;
                        TextBox1.Enabled = false;

                    }
                    else
                    {
                        Label1.Text = "El `Departamento con codigo " + TextBox13.Text + " no existe";
                    }
                    //MessageBox.Show("No existe un artículo con el código ingresado");
                    sqlCon.Close();

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        //modificar dato
        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {

                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "update Departamento set nombredepartamento='" + TextBox2.Text + "', id_ciudad=" + TextBox3.Text + "where id_departamento =" + TextBox13.Text;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(cadena, sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();

                    Label1.Text = "El Departamento con codigo " + TextBox13.Text + " fue modificado con exito";
                    TextBox1.Enabled = true;
                    eliminar();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //mostrar tabla de datos
        protected void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT id_departamento, nombredepartamento, id_ciudad FROM Departamento ", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    gvPhoneBook.DataSource = dtbl;
                    gvPhoneBook.DataBind();

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        // agregar dato
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Convert.ToInt32(DDLPuesto.Text)
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    if (string.IsNullOrEmpty(TextBox1.Text) & string.IsNullOrEmpty(TextBox2.Text) & string.IsNullOrEmpty(TextBox3.Text))
                    {
                        Label1.Text = "Algunos de los datos obligatorios no se han llenado, intentar nuevamente";
                    }
                    else
                    {
                        sqlCon.Open();
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Departamento (id_departamento, nombredepartamento, id_ciudad) VALUES (" + TextBox1.Text + " , '" + TextBox2.Text + "'," + TextBox3.Text + ")", sqlCon);
                        DataTable dtbl = new DataTable();

                        sqlDa.Fill(dtbl);
                        sqlCon.Close();

                        Label1.Text = "El Departamento " + TextBox1.Text + " fue guardado con exito";
                        eliminar();
                    }

                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //Eliminar dato
        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("Delete from Departamento WHERE id_departamento=" + TextBox12.Text, sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);


                    Label1.Text = "El Departamento " + TextBox12.Text + " fue Eliminado con exito";
                    eliminar();
                    sqlCon.Close();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }

        public void eliminar()
        {
            TextBox13.Text = " ";
            TextBox1.Text = " ";
            TextBox2.Text = " ";
            TextBox3.Text = " ";
            TextBox12.Text = " ";
            TextBox13.Text = " ";
        }
    }
}
