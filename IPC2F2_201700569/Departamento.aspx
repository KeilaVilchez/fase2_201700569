﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Departamento.aspx.cs" Inherits="IPC2F2_201700569.Departamento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    background-image: url('https://i.pinimg.com/originals/d0/71/de/d071de619f9b5f9410021c1e5dc680c1.jpg');
}

.form-register {
    width: 400px;
    background: #24303c;
    padding: 30px;
    margin: auto;
    margin-top: 100px;
    border-radius: 4px;
    font-family: 'calibri';
    color: white;
    box-shadow: 7px 13px 37px #000;
}

    .form-register h4 {
        font-size: 22px;
        margin-bottom: 20px;
    }

.controls {
    width: 100%;
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}

.controls1{
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}
.form-register p {
    height: 40px;
    text-align: center;
    font-size: 18px;
    line-height: 40px;
}

.form-register a {
    color: white;
    text-decoration: none;
}

    .form-register a:hover {
        color: white;
        text-decoration: underline;
    }

.form-register .botons {
        border-style: none;
            border-color: inherit;
            border-width: medium;
            background: #1f53c5;
            padding: 12px;
            color: white;
    margin: 0px 0 16px 0;
        }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <section class="form-register">
    <h4> Depertamento </h4>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button class="botons" ID="Button5" runat="server" Text="Buscar" Width="70px" OnClick="Button5_Click"  />
            
            
            &nbsp;&nbsp;&nbsp;
            <asp:TextBox class="controls1" ID="TextBox13" runat="server" autofocus="true"  placeholder="Ingresar Codigo " Width="146px" ></asp:TextBox>
    
            
         <asp:TextBox class="controls" ID="TextBox1" runat="server" autofocus="true"  placeholder="Ingrese Codigo de Departamento"  ></asp:TextBox>
         <asp:TextBox class="controls" ID="TextBox2" runat="server" autofocus="true"  placeholder="Ingrese nombre del Departamento" ></asp:TextBox>
         <asp:TextBox class="controls" ID="TextBox3" runat="server" autofocus="true"  placeholder="Ingrese id de la Ciudad" ></asp:TextBox>
     
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button class="botons" ID="Button3" runat="server" Text="Modificar" Width="75px" OnClick="Button3_Click" />

            <asp:Button class="botons" ID="Button1" runat="server" Text="Agregar"  Width="102px" OnClick="Button1_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
        </p><asp:Label ID="Label1" runat="server"> </asp:Label>
        <p> &nbsp;&nbsp;</p>
        
            <asp:Button class="botons" ID="Button4" runat="server" Text="Departamentos" Width="109px" OnClick="Button4_Click"  />
            &nbsp;<asp:Button class="botons" ID="Button2" runat="server" Text="Eliminar" Width="70px" OnClick="Button2_Click"  />
            
            &nbsp;
        <asp:TextBox class="controls1" ID="TextBox12" runat="server" autofocus="true"  placeholder="Ingresar codigo" Width="134px" ></asp:TextBox>
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            &nbsp;&nbsp;
                
        <div>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:GridView ID="gvPhoneBook" runat="server" AutoGenerateColumns="False" Width="334px">
            <Columns>
                <asp:BoundField DataField="id_departamento" HeaderText ="id_departamento" />
                <asp:BoundField DataField="nombredepartamento" HeaderText ="nombrdepartamento" />
                <asp:BoundField DataField="id_ciudad" HeaderText ="id_ciudad" />
               
              
            </Columns>
        </asp:GridView>
        </div>
       
    <p><a href="#">Regresar</a></p>
  </section>
    </form>
</body>
</html>
