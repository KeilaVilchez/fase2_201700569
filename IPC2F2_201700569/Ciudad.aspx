﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ciudad.aspx.cs" Inherits="IPC2F2_201700569.Ciudad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    background-image: url('https://fromthemesstothemass.files.wordpress.com/2015/01/ec_0654_venetian_causewayfl.jpg');
}

.form-register {
    width: 400px;
    background: #24303c;
    padding: 30px;
    margin: auto;
    margin-top: 100px;
    border-radius: 4px;
    font-family: 'calibri';
    color: white;
    box-shadow: 7px 13px 37px #000;
}

    .form-register h4 {
        font-size: 22px;
        margin-bottom: 20px;
    }

.controls {
    width: 100%;
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}
.controls1{
    background: #24303c;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 16px;
    border: 1px solid #1f53c5;
    font-family: 'calibri';
    font-size: 18px;
    color: white;
}
.form-register p {
    height: 40px;
    text-align: center;
    font-size: 18px;
    line-height: 40px;
}

.form-register a {
    color: white;
    text-decoration: none;
}

    .form-register a:hover {
        color: white;
        text-decoration: underline;
    }

.form-register .botons {
    width: 100%;
    background: #1f53c5;
    border: none;
    padding: 12px;
    color: white;
    margin: 16px 0;
}
</style>
</head>
<body>
     <form id="form1" runat="server">
    <section class="form-register">
    <h4> Ciudades</h4>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button class="botons" ID="Button5" runat="server" Text="Buscar" Width="70px" OnClick="Button5_Click"  />
            
            
            &nbsp;&nbsp;&nbsp;
            <asp:TextBox class="controls1" ID="TextBox13" runat="server" autofocus="true"  placeholder="Ingresar Codigo " Width="146px" ></asp:TextBox>
    
            
         <asp:TextBox class="controls" ID="TextBox1" runat="server" autofocus="true"  placeholder="Ingrese Codigo de Ciudad"  ></asp:TextBox>
         <asp:TextBox class="controls" ID="TextBox2" runat="server" autofocus="true"  placeholder="Ingrese nombre de la Ciudad" ></asp:TextBox>
      
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button class="botons" ID="Button3" runat="server" Text="Modificar" Width="75px" OnClick="Button3_Click" />

            <asp:Button class="botons" ID="Button1" runat="server" Text="Agregar"  Width="102px" OnClick="Button1_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
        </p><asp:Label ID="Label1" runat="server"> </asp:Label>
        <p> &nbsp;&nbsp;</p>
        
            <asp:Button class="botons" ID="Button4" runat="server" Text="Ciudades" Width="89px" OnClick="Button4_Click"  />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button class="botons" ID="Button2" runat="server" Text="Eliminar" Width="70px" OnClick="Button2_Click"  />
            
            &nbsp;
        <asp:TextBox class="controls1" ID="TextBox12" runat="server" autofocus="true"  placeholder="Ingresar codigo" Width="134px" ></asp:TextBox>
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            &nbsp;&nbsp;
                
        <div>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:GridView ID="gvPhoneBook" runat="server" AutoGenerateColumns="False" Width="334px">
            <Columns>
                <asp:BoundField DataField="id_ciudad" HeaderText ="id_ciudad" />
                <asp:BoundField DataField="nombreciudad" HeaderText ="nombreciudad" />
               
              
            </Columns>
        </asp:GridView>
        </div>
       
    <p><a href="#">Regresar</a></p>
  </section>
    </form>
</body>
</html>
