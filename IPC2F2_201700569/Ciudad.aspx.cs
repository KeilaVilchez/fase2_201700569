﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IPC2F2_201700569
{
    public partial class Ciudad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "SELECT id_ciudad, nombreciudad FROM Ciudad where  id_ciudad =" + TextBox13.Text;
                    SqlCommand comando = new SqlCommand(cadena, sqlCon);
                    SqlDataReader registro = comando.ExecuteReader();
                    if (registro.Read())
                    {
                        TextBox1.Text = registro["id_ciudad"].ToString();
                        TextBox2.Text = registro["nombreciudad"].ToString();
                        Button3.Enabled = true;
                        TextBox1.Enabled = false;

                    }
                    else
                        //MessageBox.Show("No existe un artículo con el código ingresado");
                        sqlCon.Close();

                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //modificar dato
        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {

                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string cadena = "update Ciudad set nombreciudad='" + TextBox2.Text + "' where id_ciudad =" + TextBox13.Text;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(cadena, sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();

                    Label1.Text = "La ciudad con codigo " + TextBox13.Text + " fue modificado con exito";
                    TextBox1.Enabled = true;
                    eliminar();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //mostrar tabla de datos
        protected void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT id_ciudad, nombreciudad FROM Ciudad ", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    gvPhoneBook.DataSource = dtbl;
                    gvPhoneBook.DataBind();

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        // agregar dato
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Convert.ToInt32(DDLPuesto.Text)
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    if (string.IsNullOrEmpty(TextBox1.Text) & string.IsNullOrEmpty(TextBox2.Text))
                    {
                        Label1.Text = "Algunos de los datos obligatorios no se han llenado, intentar nuevamente";
                    }
                    else
                    {
                        sqlCon.Open();
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Ciudad (id_ciudad, nombreciudad) VALUES (" + TextBox1.Text + " , '" + TextBox2.Text + "')", sqlCon);
                        DataTable dtbl = new DataTable();

                        sqlDa.Fill(dtbl);
                        sqlCon.Close();

                        Label1.Text = "La Ciudad" + TextBox1.Text + " fue guardado con exito";
                        eliminar();
                    }

                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //Eliminar dato
        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = @"Data Source=LAPTOP-O3BQ0BRC; Initial Catalog = Proyecto1_IPC2; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("Delete from Ciudad WHERE id_ciudad=" + TextBox12.Text, sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);


                    Label1.Text = "La ciudad" + TextBox12.Text + " fue Eliminado con exito";
                    eliminar();
                    sqlCon.Close();
                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
        }
        //vaciar datos
        public void eliminar()
        {
            TextBox13.Text = " ";
            TextBox1.Text = " ";
            TextBox2.Text = " ";
            TextBox12.Text = " ";
            TextBox13.Text = " ";
        }
    }
}